package com.ia.test.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by robertog on 4/08/16.
 */
public class Branches {

    @SerializedName("Id")
    int id;
    @SerializedName("IdComplejoVista")
    int idComplejoVista;
    @SerializedName("IdCiudad")
    int idCiudad;
    @SerializedName("Nombre")
    String nombre;
    @SerializedName("Latitud")
    double latitud;
    @SerializedName("Longitud")
    double longitud;
    @SerializedName("telefono")
    int telefono;
    @SerializedName("Direccion")
    String direccion;
    @SerializedName("EsCompraAnticipada")
    boolean compraAnticipada;
    @SerializedName("EsReservaPermitida")
    boolean reservaPermitida;
    @SerializedName("UriSitio")
    String uriSitio;
    @SerializedName("Transporte")
    String transporte;

    public Branches(int id, int idComplejoVista, int idCiudad, String nombre,
                    double latitud, double longitud, int telefono, String direccion,
                    boolean compraAnticipada, boolean reservaPermitida, String uriSitio,
                    String transporte) {

        this.id = id;
        this.idComplejoVista = idComplejoVista;
        this.idCiudad = idCiudad;
        this.nombre = nombre;
        this.latitud = latitud;
        this.longitud = longitud;
        this.telefono = telefono;
        this.direccion = direccion;
        this.compraAnticipada = compraAnticipada;
        this.reservaPermitida = reservaPermitida;
        this.uriSitio = uriSitio;
        this.transporte = transporte;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdComplejoVista() {
        return idComplejoVista;
    }

    public void setIdComplejoVista(int idComplejoVista) {
        this.idComplejoVista = idComplejoVista;
    }

    public int getIdCiudad() {
        return idCiudad;
    }

    public void setIdCiudad(int idCiudad) {
        this.idCiudad = idCiudad;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getLatitud() {
        return latitud;
    }

    public void setLatitud(double latitud) {
        this.latitud = latitud;
    }

    public double getLongitud() {
        return longitud;
    }

    public void setLongitud(double longitud) {
        this.longitud = longitud;
    }

    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public boolean isCompraAnticipada() {
        return compraAnticipada;
    }

    public void setCompraAnticipada(boolean compraAnticipada) {
        this.compraAnticipada = compraAnticipada;
    }

    public boolean isReservaPermitida() {
        return reservaPermitida;
    }

    public void setReservaPermitida(boolean reservaPermitida) {
        this.reservaPermitida = reservaPermitida;
    }

    public String getUriSitio() {
        return uriSitio;
    }

    public void setUriSitio(String uriSitio) {
        this.uriSitio = uriSitio;
    }

    public String getTransporte() {
        return transporte;
    }

    public void setTransporte(String transporte) {
        this.transporte = transporte;
    }
}
