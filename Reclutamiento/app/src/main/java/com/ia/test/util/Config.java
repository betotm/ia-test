package com.ia.test.util;

/**
 * Created by robertog on 4/08/16.
 */
public class Config {

    //region Urls
    public static final String CITIES_URL = "http://api.cinepolis.com.mx/Consumo.svc/json/ObtenerCiudades";
    public static final String BRANCHES_URL = "http://api.cinepolis.com.mx/sqlite.aspx?idCiudad=";
    //endregion

    //region DownloadRequestCodes
    public static final int CODE_DEFAULT = 0;
    public static final int CODE_CITIES_REQUEST = 1;
    public static final int CODE_BRANCHES_REQUEST = 2;
    //endregion

    //region Tags for Arguments or Preferences
    public static final String TAG_CODE = "TAG_CODE";
    public static final String TAG_CITY_ID = "TAG_CITY_ID";
    public static final String TAG_LAT = "TAG_LAT";
    public static final String TAG_LONG = "TAG_LONG";


}
