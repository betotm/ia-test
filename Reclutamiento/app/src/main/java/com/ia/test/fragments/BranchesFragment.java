package com.ia.test.fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.UiThread;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.ia.test.R;
import com.ia.test.adapters.BranchesAdapter;
import com.ia.test.bases.BaseFragment;
import com.ia.test.events.Data;
import com.ia.test.models.Branches;
import com.ia.test.services.DownloadData;
import com.ia.test.util.Config;
import com.ia.test.util.DataBaseHelper;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;

/**
 * Created by robertog on 4/08/16.
 */
public class BranchesFragment extends BaseFragment {

    int cityId;
    ProgressDialog progressDialog;
    SQLiteDatabase mDb;
    DataBaseHelper helper;

    ArrayList<Branches> branches = new ArrayList<>();

    ListView list;
    private BranchesAdapter adapter;

    FragmentManager fragmentManager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_branches_list, container, false);

        fragmentManager = getFragmentManager();

        cityId = getArguments().getInt(Config.TAG_CITY_ID, 0);

        list = (ListView) root.findViewById(R.id.list_branches);

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage(getResources().getString(R.string.message_info_downloading));
        progressDialog.setCancelable(false);
        progressDialog.show();

        getInfo();

        setHasOptionsMenu(true);

        return root;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_map, menu);
        MenuItem item = menu.findItem(R.id.menu_item_list);
        if (item.isVisible())
            item.setVisible(false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.menu_item_map) {

            Fragment fragment = new MapsFragment();
            Bundle args = new Bundle();
            args.putInt(Config.TAG_CITY_ID, cityId);
            fragment.setArguments(args);

            fragmentManager.beginTransaction().add(R.id.fragment_container, fragment, null)
                    .addToBackStack(null).commit();
        }

        return super.onOptionsItemSelected(item);
    }

    @UiThread
    private void getInfo() {
        if (cityId != 0) {
            Intent intent = new Intent(getActivity(), DownloadData.class);
            intent.putExtra(Config.TAG_CODE, Config.CODE_BRANCHES_REQUEST);
            intent.putExtra(Config.TAG_CITY_ID, cityId);
            getActivity().startService(intent);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(Data.EventDataBaseDownloaded event) {

        if (event.isSuccess()) {

            helper = new DataBaseHelper(getActivity(), event.getFilePath().getPath());

            if (helper.openDataBase() != null) {
                serializeBranchesData(helper.query("*", "Complejo"));

                adapter = new BranchesAdapter(branches, getActivity());
                list.setAdapter(adapter);
            }

        } else sT(R.string.message_error_download_info);

        if (progressDialog.isShowing())
            progressDialog.dismiss();
    }

    private void serializeBranchesData(Cursor row) {

        if (row != null) {
            row.moveToFirst();
            while (!row.isAfterLast()){

                branches.add(new Branches(
                        row.getInt(0),
                        row.getInt(1),
                        row.getInt(2),
                        row.getString(3),
                        row.getDouble(4),
                        row.getDouble(5),
                        row.getInt(6),
                        row.getString(7),
                        row.getInt(8) != 0,
                        row.getInt(9) != 0,
                        row.getString(10),
                        row.getString(11))
                );

                row.moveToNext();
            }
        }
    }
}
