package com.ia.test.fragments;


import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.UiThread;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.ia.test.R;
import com.ia.test.bases.BaseFragment;
import com.ia.test.events.Data;
import com.ia.test.models.Branches;
import com.ia.test.services.DownloadData;
import com.ia.test.util.Config;
import com.ia.test.util.DataBaseHelper;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;

public class MapsFragment extends BaseFragment implements OnMapReadyCallback {

    private GoogleMap mMap;
    private DataBaseHelper helper;
    private ProgressDialog progressDialog;
    private ArrayList<Branches> branches = new ArrayList<>();
    private Location mLocation;

    int cityId = -1;
    private LatLng latLng;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_maps, container, false);

        cityId = getArguments().getInt(Config.TAG_CITY_ID, Config.CODE_DEFAULT);

//        progressDialog = new ProgressDialog(getActivity());
//        progressDialog.setMessage(getResources().getString(R.string.message_info_downloading));
//        progressDialog.setCancelable(false);
//        progressDialog.show();

        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        setHasOptionsMenu(true);
        return root;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_map, menu);

        MenuItem item = menu.findItem(R.id.menu_item_map);
        if (item.isVisible())
            item.setVisible(false);
        getActivity().invalidateOptionsMenu();

    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {

        super.onPrepareOptionsMenu(menu);

        MenuItem item = menu.findItem(R.id.menu_item_map);
        if (item.isVisible())
            item.setVisible(false);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.menu_item_list) {

            getFragmentManager().popBackStack();
        }

        return super.onOptionsItemSelected(item);
    }

    @UiThread
    private void getInfo() {
        if (cityId != 0) {
            Intent intent = new Intent(getActivity(), DownloadData.class);
            intent.putExtra(Config.TAG_CODE, Config.CODE_BRANCHES_REQUEST);
            intent.putExtra(Config.TAG_CITY_ID, cityId);
            getActivity().startService(intent);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.getUiSettings().setZoomControlsEnabled(false);
        mMap.getUiSettings().setZoomGesturesEnabled(true);

        mLocation = initLocation();

        if (mLocation == null)
            mLocation = new Location("0,0");

        latLng = new LatLng(mLocation.getLatitude(), mLocation.getLongitude());
        CameraUpdate camUpd = CameraUpdateFactory.newLatLngZoom(latLng, 15F);
        mMap.moveCamera(camUpd);

        googleMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

            @Override
            public View getInfoWindow(Marker arg0) {
                return null;
            }

            @Override
            public View getInfoContents(Marker arg0) {

                View v = getLayoutInflater(null).inflate(R.layout.info_window_layout, null);


                TextView txtName = (TextView) v.findViewById(R.id.txt_info_name);
                TextView txtAddress = (TextView) v.findViewById(R.id.txt_info_address);

                Branches singleBranch = branches.get(0);

                for (Branches b : branches)
                    if (b.getNombre().equals(arg0.getTitle())) {
                        singleBranch = b;
                        break;
                    }

                txtName.setText(singleBranch.getNombre());
                txtAddress.setText(singleBranch.getDireccion());

                return v;

            }
        });

        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {

                MarkerOptions markerOptions = new MarkerOptions();

                markerOptions.position(marker.getPosition());

                mMap.animateCamera(CameraUpdateFactory.newLatLng(marker.getPosition()));

                marker.showInfoWindow();

                return false;
            }
        });

        if (ActivityCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(true);

        getInfo();
    }

    private Location initLocation() {
        LocationManager locationManager = (LocationManager) getActivity().getSystemService(getActivity().LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            return null;
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1,
                1, new LocationListener() {
                    @Override
                    public void onLocationChanged(Location location) {

                    }

                    @Override
                    public void onStatusChanged(String provider, int status, Bundle extras) {

                    }

                    @Override
                    public void onProviderEnabled(String provider) {

                    }

                    @Override
                    public void onProviderDisabled(String provider) {

                    }
                });

        return locationManager.getLastKnownLocation(/*LocationManager.GPS_PROVIDER,*/LocationManager.NETWORK_PROVIDER);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(Data.EventDataBaseDownloaded event) {

        if (event.isSuccess()) {

            helper = new DataBaseHelper(getActivity(), event.getFilePath().getPath());

            if (helper.openDataBase() != null) {
                serializeBranchesData(helper.query("*", "Complejo"));

                for (Branches branch : branches) {

                    LatLng latLng = new LatLng(branch.getLatitud(), branch.getLongitud());
                    mMap.addMarker(new MarkerOptions().position(latLng).title(branch.getNombre()));
                }
            }

        } else sT(R.string.message_error_download_info);

        if (progressDialog.isShowing())
            progressDialog.dismiss();
    }

    private void serializeBranchesData(Cursor row) {

        if (row != null) {
            row.moveToFirst();
            while (!row.isAfterLast()){

                branches.add(new Branches(
                        row.getInt(0),
                        row.getInt(1),
                        row.getInt(2),
                        row.getString(3),
                        row.getDouble(4),
                        row.getDouble(5),
                        row.getInt(6),
                        row.getString(7),
                        row.getInt(8) != 0,
                        row.getInt(9) != 0,
                        row.getString(10),
                        row.getString(11))
                );

                row.moveToNext();
            }
        }
    }
}
