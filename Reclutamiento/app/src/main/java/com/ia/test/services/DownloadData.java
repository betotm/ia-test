package com.ia.test.services;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.ia.test.events.Data;
import com.ia.test.models.Cities;
import com.ia.test.util.Config;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.greenrobot.eventbus.EventBus;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;

public class DownloadData extends IntentService {

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     * <p/>
     * //     * @param name Used to name the worker thread, important only for debugging.
     * //
     */
    public DownloadData() {
        super("DownloadData");
    }

    private String BASE_PATH;

    private OkHttpClient client;
    Request request;
    int requestCode = 0;
    int idCity = 0;
    String url = "";


    @Override
    protected void onHandleIntent(Intent intent) {

        BASE_PATH = getExternalFilesDir(null) + File.separator + "database_";

        requestCode = intent.getIntExtra(Config.TAG_CODE, Config.CODE_DEFAULT);
        idCity = intent.getIntExtra(Config.TAG_CITY_ID, -1);

        File file;

        if (idCity != -1) {
            file = new File(BASE_PATH + idCity + ".sqlite");
            if (!file.exists())
                startDownload();
            else {
                try {
                    notifyResponse(null);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } else if (requestCode == Config.CODE_CITIES_REQUEST)
            startDownload();
    }

    private void startDownload() {
        client = new OkHttpClient();
        try {
            url = getUrl();

            request = new Request.Builder()
                    .url(url)
                    .build();

            Log.d("REQUEST", request.toString());
            Response response = client.newCall(request).execute();

            if (!response.isSuccessful()) {

                Log.d("FILE NOT DOWNLOADED", "***************onFail**************");

                throw new IOException("Unexpected code " + response);
            } else {

                Log.d("FILE DOWNLOADED", "***************onSuccess**************");

                notifyResponse(response);

            }
        } catch (UnsupportedEncodingException e) {
            Log.e("utf8", "conversion", e);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void notifyResponse(Response response) throws IOException{

        if (response == null) {
            EventBus.getDefault().post(new Data.EventDataBaseDownloaded(
                    new File(BASE_PATH + idCity + ".sqlite"), true));
            return;
        }

        switch (requestCode) {

            case Config.CODE_CITIES_REQUEST:
                String jsonResponse = response.body().string();

                Type type = new TypeToken<Collection<Cities>>() {}.getType();
                ArrayList<Cities> citiesList = new Gson().fromJson(jsonResponse, type);
                EventBus.getDefault().post(new Data.EventCitiesDownloaded(citiesList, true));

                break;

            case Config.CODE_BRANCHES_REQUEST:

                File file = new File(BASE_PATH + idCity + ".sqlite");
                InputStream is = response.body().byteStream();
                BufferedInputStream input = new BufferedInputStream(is);
                OutputStream output = new FileOutputStream(file);

                byte[] data = new byte[1024];

                int count;
                while ((count = input.read(data)) != -1) {
                    output.write(data, 0, count);
                }

                EventBus.getDefault().post(new Data.EventDataBaseDownloaded(file, true));

                break;
        }
    }

    private String getUrl() {

        switch (requestCode) {
            case Config.CODE_CITIES_REQUEST:
                return Config.CITIES_URL;

            case Config.CODE_BRANCHES_REQUEST:
                return Config.BRANCHES_URL + idCity;

            default:
                return null;
        }
    }
}
