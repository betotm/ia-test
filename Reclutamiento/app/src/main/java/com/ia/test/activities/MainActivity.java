package com.ia.test.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.UiThread;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.ia.test.adapters.CitiesAdapter;
import com.ia.test.bases.BaseActivity;
import com.ia.test.events.Data;
import com.ia.test.fragments.BranchesFragment;
import com.ia.test.models.Cities;
import com.ia.test.services.DownloadData;
import com.ia.test.R;
import com.ia.test.util.Config;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends BaseActivity implements AdapterView.OnItemClickListener{

    List<Cities> cities = new ArrayList<>();
    CitiesAdapter adapter;

    ListView list;

    FragmentManager fragmentManager;

    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        fragmentManager = getSupportFragmentManager();

        initViews();
        getInfo();
    }

    private void initViews() {
        list = (ListView) findViewById(R.id.list_cities);
        list.setOnItemClickListener(this);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getResources().getString(R.string.message_info_downloading));
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @UiThread
    public void getInfo(){
        Intent intent = new Intent(this, DownloadData.class);
        intent.putExtra(Config.TAG_CODE, Config.CODE_CITIES_REQUEST);
        startService(intent);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(Data.EventCitiesDownloaded event) {

        if (event.isSuccess()) {

            for (Cities city : event.getCities())
                cities.add(city);

            adapter = new CitiesAdapter(cities, this);
            list.setAdapter(adapter);

        } else sT(R.string.message_error_download_cities);

        if (progressDialog.isShowing())
            progressDialog.dismiss();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        Fragment fragment = new BranchesFragment();
        Bundle args = new Bundle();
        args.putInt(Config.TAG_CITY_ID, adapter.getItem(position).getId());
        fragment.setArguments(args);

        fragmentManager.beginTransaction().add(R.id.fragment_container, fragment, null)
                .addToBackStack(null).commit();

    }
}
