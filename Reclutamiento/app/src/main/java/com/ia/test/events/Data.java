package com.ia.test.events;

import com.ia.test.models.Cities;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by robertog on 4/08/16.
 */
public class Data {

    public static class EventCitiesDownloaded {

        ArrayList<Cities> cities;
        boolean success;

        public EventCitiesDownloaded(ArrayList<Cities> cities, boolean success) {
            this.cities = cities;
            this.success = success;
        }

        public ArrayList<Cities> getCities() {
            return cities;
        }

        public boolean isSuccess() {
            return success;
        }
    }

    public static class EventDataBaseDownloaded {

        File filePath;
        boolean success;

        public EventDataBaseDownloaded(File filePath, boolean success) {
            this.filePath = filePath;
            this.success = success;
        }

        public File getFilePath() {
            return filePath;
        }

        public boolean isSuccess() {
            return success;
        }
    }
}
