package com.ia.test.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by robertog on 4/08/16.
 */
public class Cities {

    @SerializedName("Estado")
    String estado;
    @SerializedName("Id")
    int id;
    @SerializedName("IdEstado")
    int idEstado;
    @SerializedName("IdPais")
    int idPais;
    @SerializedName("Latitud")
    double latitud;
    @SerializedName("Longitud")
    double longitud;
    @SerializedName("Nombre")
    String nombre;
    @SerializedName("Pais")
    String pais;
    @SerializedName("Uris")
    String uris;

    public Cities() {
    }

    public Cities(String estado, int id, int idEstado, int idPais, double latitud, double longitud,
                  String nombre, String pais, String uris) {
        this.estado = estado;
        this.id = id;
        this.idEstado = idEstado;
        this.idPais = idPais;
        this.latitud = latitud;
        this.longitud = longitud;
        this.nombre = nombre;
        this.pais = pais;
        this.uris = uris;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(int idEstado) {
        this.idEstado = idEstado;
    }

    public int getIdPais() {
        return idPais;
    }

    public void setIdPais(int idPais) {
        this.idPais = idPais;
    }

    public double getLatitud() {
        return latitud;
    }

    public void setLatitud(double latitud) {
        this.latitud = latitud;
    }

    public double getLongitud() {
        return longitud;
    }

    public void setLongitud(double longitud) {
        this.longitud = longitud;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getUris() {
        return uris;
    }

    public void setUris(String uris) {
        this.uris = uris;
    }
}
