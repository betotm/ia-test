package com.ia.test.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ia.test.R;
import com.ia.test.models.Cities;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by robertog on 4/08/16.
 */
public class CitiesAdapter extends BaseAdapter {

    List<Cities> citiesList = new ArrayList<>();
    Context context;
    private LayoutInflater inflater;
    int size;

    public CitiesAdapter(List<Cities> citiesList, Context context) {
        this.context = context;

        for (Cities item : citiesList)
            this.citiesList.add(item);

        size = this.citiesList.size();
    }

    public void updateDataSet(List<Cities> cities) {

        if (citiesList.size() > 0)
            citiesList.clear();

        for (Cities city : cities)
            citiesList.add(city);
    }

    @Override
    public int getCount() {
        return this.size;
    }

    @Override
    public Cities getItem(int position) {
        return citiesList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.row_cities, parent, false);

        Cities city = getItem(position);

        TextView txtName = (TextView) view.findViewById(R.id.txt_citys_name);
        txtName.setText(city.getNombre());

        return view;
    }
}
