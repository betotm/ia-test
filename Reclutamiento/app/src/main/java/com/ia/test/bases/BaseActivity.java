package com.ia.test.bases;

import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by robertog on 4/08/16.
 */
public class BaseActivity extends AppCompatActivity {

    @Override
    protected void onStart() {
        super.onStart();
        if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().unregister(this);
    }

    public void sT(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    public void sT(int message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    public void lT(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    public void lT(int message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }
}
