package com.ia.test.util;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


/**
 * Created by robertog on 4/08/16.
 */
public class DataBaseHelper extends SQLiteOpenHelper {

    private SQLiteDatabase myDataBase;
    private String DB_NAME;

    public DataBaseHelper(Context context, String DB_NAME) {
        super(context, DB_NAME, null, 1);
        this.DB_NAME = DB_NAME;
    }

    public SQLiteDatabase openDataBase() throws SQLException {
        myDataBase = SQLiteDatabase.openDatabase(DB_NAME, null, SQLiteDatabase.OPEN_READONLY);
        return myDataBase;
    }

    public Cursor query(String column, String table) {
        String query = "SELECT " + column + " From " + table;
        return myDataBase.rawQuery(query, null);
    }

    @Override
    public synchronized void close() {

        if(myDataBase != null)
            myDataBase.close();

        super.close();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }
}
