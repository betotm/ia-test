package com.ia.test.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ia.test.R;
import com.ia.test.models.Branches;
import com.ia.test.models.Cities;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by robertog on 4/08/16.
 */
public class BranchesAdapter extends BaseAdapter {

    List<Branches> branches = new ArrayList<>();
    Context context;
    private LayoutInflater inflater;
    int size;

    public BranchesAdapter(List<Branches> branches, Context context) {
        this.context = context;

        for (Branches item : branches)
            this.branches.add(item);

        size = this.branches.size();
    }

    public void updateDataSet(List<Branches> branches) {

        if (branches.size() > 0)
            branches.clear();

        for (Branches branch : branches)
            branches.add(branch);
    }

    @Override
    public int getCount() {
        return this.size;
    }

    @Override
    public Branches getItem(int position) {
        return branches.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.row_branches, parent, false);

        Branches branches = getItem(position);

        TextView txtName = (TextView) view.findViewById(R.id.txt_branch_name);
        TextView txtAddress = (TextView) view.findViewById(R.id.txt_branch_address);
        txtName.setText(branches.getNombre());
        txtAddress.setText(branches.getDireccion());

        return view;
    }
}
