package com.ia.test.bases;

import android.support.v4.app.Fragment;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by robertog on 4/08/16.
 */
public class BaseFragment extends Fragment {

    @Override
    public void onStart() {
        super.onStart();
        if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().unregister(this);
    }

    public void sT(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    public void sT(int message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    public void lT(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
    }

    public void lT(int message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
    }

}
